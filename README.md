# Install Foton for Laravel4

## Add package (/composer.json)
    
    "require": {
        "foton/framework": "2.0.*",
    },
    "repositories": [
        {
            "type":"git",
            "url": "https://zb3k@bitbucket.org/zb3k/foton2.git"
        }
    ],

Update packages:

    $> composer update



## Add artisan commands (/app/start/artisan.php)
    
    Artisan::add(new Foton\Install\InstallCommand);
    Artisan::add(new Foton\Component\CreateCommand);



## 1. Automatic install

    $> php artisan foton:install


## 2. Manual install


### Set aliases (/app/config/app.php)

Change:

    'App'        => 'Foton\Framework\App',
    'Config'     => 'Foton\Framework\Config',
    'Controller' => 'Foton\Framework\Controller',
    'Route'      => 'Foton\Framework\Route',
    'View'       => 'Foton\Framework\View',

Add new:

    'Model' => 'Foton\Framework\Model',
    'Foton' => 'Foton\Framework\Framework',



### Add framework init (/app/routes.php)

    Foton::setRoutes();



### Add config (/app/config/foton.php) : optional

    <?php return array(
        'route_all_components' => true,
        'home_component'       => 'home',
    );



### Create home component

    $> php artisan foton:component home
    $> composer dumpautoload



### Publish foton/framework package

    php artisan asset:publish foton/framework



# Twitter Bootstrap

## Add package to composer.json

    "twitter/bootstrap": "2.*"



## Update packages

    $> composer update



## Publish package

    $> php artisan asset:publish twitter/bootstrap --path=vendor/twitter/bootstrap/docs/assets/


