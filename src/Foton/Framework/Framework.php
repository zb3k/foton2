<?php namespace Foton\Framework;

use Foton\Framework\Framework as Foton;

class Framework
{
	//--------------------------------------------------------------------------

	protected static $config          = null;
	protected static $config_defaults = array(
		'route_all_components' => false,
		'home_component'       => '',
		'default_layout'       => false,
	);

	//--------------------------------------------------------------------------

	public static function config($key)
	{
		if (is_null(self::$config))
		{
			self::$config = self::$config_defaults;

			$config = Config::get('foton');
			foreach ($config as $key => $val)
			{
				self::$config[$key] = $val;
			}
		}

		return isset(self::$config[$key]) ? self::$config[$key] : null;
	}

	//--------------------------------------------------------------------------

	// Add routes for each components controller;
	public static function setRoutes()
	{
		// self::init();

		if (self::config('home_component'))
		{
			$controller = Foton::getComponentClass(Foton::config('home_component'), 'controller');
			Route::get('/', $controller . '@getIndex');
		}

		if (self::config('route_all_components'))
		{
			$files = scandir(FF_COM_PATH);
			foreach ($files as $com)
			{
				if ($com{0} == '.' || $com{0} == '_' || ! is_dir(FF_COM_PATH . $com)) continue;

				$controller = Foton::getComponentClass($com, 'controller');

				if (file_exists(FF_COM_PATH . $com . '/' . $controller . '.php'))
				{
					Route::controller($com, $controller);
				}
			}
		}
	}

	//--------------------------------------------------------------------------

	public static function getComponentClass($com, $type = null)
	{
		$comClass = 'Com' . ucfirst($com);

		switch ($type)
		{
			case 'controller':
				return $comClass . 'Controller';

			case 'model':
				return $comClass . 'Model';

			case null:
				return $comClass;
		}
	}

	//--------------------------------------------------------------------------


}