<?php namespace Foton\Framework;

use Foton\Framework\Framework as Foton;

class Controller extends \Illuminate\Routing\Controllers\Controller
{
	//--------------------------------------------------------------------------

	protected $comName;
	protected $layout;

	//--------------------------------------------------------------------------

	public function __construct()
	{
		$class = get_class($this);

		// set component name by classname
		if ( empty($this->comName))
		{
			$this->comName = strtolower(preg_replace('@^Com(.*)Controller$@i', '$1', $class));
		}

		// Set default layout
		$this->layout = Foton::config('default_layout');

		// Controller object as singleton
		App::singleton($class, $this);

		// Add view namespace for this component
		View::addNamespace('com.' . $this->comName, FF_COM_PATH . $this->comName . '/views');
	}

	//--------------------------------------------------------------------------

	protected function view($name)
	{
		return View::make('com.' . $this->comName . '::' . $name);
	}

	//--------------------------------------------------------------------------

	protected function directCallAction($method, $parameters)
	{
		// print_r($method, $parameters);
		if ( ! is_null($this->layout)) {
			$this->layout->content = parent::directCallAction($method, $parameters);
		}
		else
		{
			return parent::directCallAction($method, $parameters);
		}

	}

	//--------------------------------------------------------------------------

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	//--------------------------------------------------------------------------

}