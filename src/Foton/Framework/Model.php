<?php namespace Foton\Framework;


class Model extends \Illuminate\Database\Eloquent\Model
{

	public function __construct()
	{
		if ( is_null($this->table) )
		{
			$this->table = preg_replace('@_model$@', '', snake_case(class_basename($this)));
		}
	}

}