<?php namespace Foton\Component;

use Illuminate\Filesystem\Filesystem;
use Foton\Framework\Framework as Foton;

class ComponentGenerator {

	protected $files;

	//--------------------------------------------------------------------------

	public function __construct()
	{
		$this->files = new Filesystem;
	}

	//--------------------------------------------------------------------------

	public function make($comName)
	{
		$comPath           = FF_COM_PATH . $comName;
		$comViewsPath      = $comPath . "/views/";
		$comAdminPath      = $comPath . "/admin/";
		$comMigrationsPath = $comPath . "/migrations/";

		$comController = Foton::getComponentClass($comName, 'controller');
		$comModel      = Foton::getComponentClass($comName, 'model');

		$comControllerFile = $comPath . '/' . $comController . '.php';
		$comModelFile      = $comPath . '/' . $comModel . '.php';
		$comViewIndexFile  = $comViewsPath . 'index.php';

		$options = array(
			'{{comName}}'       => $comName,
			'{{comController}}' => $comController,
			'{{comModel}}'      => $comModel,

			'{{comModelFile}}'      => str_replace(FF_COM_PATH, FF_COM_FOLDER . '/', $comModelFile),
			'{{comControllerFile}}' => str_replace(FF_COM_PATH, FF_COM_FOLDER . '/', $comControllerFile),
			'{{comViewIndexFile}}'  => str_replace(FF_COM_PATH, FF_COM_FOLDER . '/', $comViewIndexFile),
		);

		$this->files->makeDirectory($comPath);
		$this->files->makeDirectory($comViewsPath);
		$this->files->makeDirectory($comAdminPath);
		$this->files->makeDirectory($comMigrationsPath);

		$this->files->put($comControllerFile, $this->makeStubs('controller', $comName, $options));
		$this->files->put($comModelFile, $this->makeStubs('model', $comName, $options));
		$this->files->put($comViewIndexFile, $this->makeStubs('view_index', $comName, $options));
	}

	//--------------------------------------------------------------------------

	protected function makeStubs($stub, $comName, $options)
	{
		$stub = $this->files->get(__DIR__.'/stubs/'.$stub.'.php');

		return str_replace(array_keys($options), $options, $stub);
	}

	//--------------------------------------------------------------------------


}